Debian-docker
=============

The image is built using image base from [docker debian][1] on docker hub

In this image:
- The packages installed include: [oracle-java-8][2], nano, net-tools.

Size of image:
- 650 MB

## Build
```
docker build -t dungcv/debian-java .
```

## Usage
```
docker run -it --rm dungcv/debian-java bash
```

## List environment variables
```
JAVA_HOME=/opt/jdk
PATH=$JAVA_HOME/bin:$PATH
```

## Done

[1]: https://hub.docker.com/_/debian/
[2]: https://www.oracle.com/java/index.html

